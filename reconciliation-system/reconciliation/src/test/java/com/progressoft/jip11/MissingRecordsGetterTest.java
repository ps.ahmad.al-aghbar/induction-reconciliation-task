package com.progressoft.jip11;

import com.progressoft.jip11.entity.FileRecord;
import com.progressoft.jip11.entity.TransactionRecord;
import com.progressoft.jip11.exceptions.EmptyTransactionsException;
import com.progressoft.jip11.recordmatcher.MissingRecordsHandler;
import com.progressoft.jip11.recordmatcher.matchingimpl.MissingRecordsGetter;
import com.progressoft.jip11.utility.DateUtility;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;

public class MissingRecordsGetterTest {
    MissingRecordsHandler missingRecords;

    @BeforeEach
    public void setup() {
        missingRecords = new MissingRecordsGetter();
        CurrencyStore.getCurrencyStore().addToCurrencySpecification("USD", 2);
        CurrencyStore.getCurrencyStore().addToCurrencySpecification("JOD", 3);
    }

    @Test
    public void canCreate() {
        new MissingRecordsGetter();
    }

    @Test
    public void givenNullHashMaps_whenGetMissingRecords_throwNullPointerException() {
        NullPointerException nullPointerException = Assertions.assertThrows(NullPointerException.class,
                () -> missingRecords.getMissingRecords(null, null));
        Assertions.assertEquals(nullPointerException.getMessage(), "Null transactions");
    }

    @Test
    public void givenEmptyHashMap_whenGetMissingRecords_throwEmptyTransactionsException() {
        EmptyTransactionsException emptyTransactionsException = Assertions.assertThrows(EmptyTransactionsException.class,
                () -> missingRecords.getMissingRecords(new LinkedHashMap<>(), new LinkedHashMap<>()));
        Assertions.assertEquals(emptyTransactionsException.getMessage(), "Empty transactions");
    }

    @Test
    public void givenValidData_whenGetMissingRecords_thenResultIsReturned() {
        HashMap<String, TransactionRecord> transactionRecordHashMapSource = new LinkedHashMap<>();
        HashMap<String, TransactionRecord> transactionRecordHashMapTarget = new LinkedHashMap<>();

        generateData(transactionRecordHashMapSource, transactionRecordHashMapTarget);

        ArrayList<FileRecord> fileRecords = missingRecords.getMissingRecords(transactionRecordHashMapSource, transactionRecordHashMapTarget);
        String datePattern = "dd/MM/yyyy";


        Assertions.assertNotNull(fileRecords);
        Assertions.assertEquals(fileRecords.size(), 4);

        Assertions.assertNotNull(fileRecords.get(0));
        Assertions.assertEquals(fileRecords.get(0).getId(), "TR-47884222201");
        Assertions.assertEquals(fileRecords.get(0).getAmount(), new BigDecimal("140.00"));
        Assertions.assertEquals(fileRecords.get(0).getDate(), DateUtility.parseDate("20/01/2020", datePattern));
        Assertions.assertEquals(fileRecords.get(0).getCurrency(), "USD");
        Assertions.assertEquals(fileRecords.get(0).getFileTag(), "SOURCE");

        Assertions.assertNotNull(fileRecords.get(1));
        Assertions.assertEquals(fileRecords.get(1).getId(), "TR-4788422220322");
        Assertions.assertEquals(fileRecords.get(1).getAmount(), new BigDecimal("5000.000"));
        Assertions.assertEquals(fileRecords.get(1).getDate(), DateUtility.parseDate("25/01/2020", datePattern));
        Assertions.assertEquals(fileRecords.get(1).getCurrency(), "JOD");
        Assertions.assertEquals(fileRecords.get(1).getFileTag(), "SOURCE");

        Assertions.assertNotNull(fileRecords.get(2));
        Assertions.assertEquals(fileRecords.get(2).getId(), "TR-478842222022245");
        Assertions.assertEquals(fileRecords.get(2).getAmount(), new BigDecimal("170.00"));
        Assertions.assertEquals(fileRecords.get(2).getDate(), DateUtility.parseDate("29/01/2020", datePattern));
        Assertions.assertEquals(fileRecords.get(2).getCurrency(), "USD");
        Assertions.assertEquals(fileRecords.get(2).getFileTag(), "TARGET");

        Assertions.assertNotNull(fileRecords.get(3));
        Assertions.assertEquals(fileRecords.get(3).getId(), "TR-47884222203");
        Assertions.assertEquals(fileRecords.get(3).getAmount(), new BigDecimal("5000.00"));
        Assertions.assertEquals(fileRecords.get(3).getDate(), DateUtility.parseDate("27/01/2020", datePattern));
        Assertions.assertEquals(fileRecords.get(3).getCurrency(), "USD");
        Assertions.assertEquals(fileRecords.get(3).getFileTag(), "TARGET");


    }

    private void generateData(HashMap<String, TransactionRecord> transactionRecordHashMapSource, HashMap<String, TransactionRecord> transactionRecordHashMapTarget) {
        String datePattern = "dd/MM/yyyy";

        transactionRecordHashMapSource.put("TR-47884222201",
                new TransactionRecord.Builder("TR-47884222201", new BigDecimal("140"), "USD"
                        , DateUtility.parseDate("20/01/2020", datePattern))
                        .setPurpose("donation")
                        .build());

        transactionRecordHashMapSource.put("TR-47884222202",
                new TransactionRecord.Builder("TR-47884222202", new BigDecimal("40"), "JOD"
                        , DateUtility.parseDate("2020-01-22", "yyyy-MM-dd"))
                        .setPurpose("donation")
                        .build());

        transactionRecordHashMapSource.put("TR-4788422220322",
                new TransactionRecord.Builder("TR-4788422220322", new BigDecimal("5000"), "JOD"
                        , DateUtility.parseDate("25/01/2020", datePattern))
                        .setPurpose("not specified")
                        .build());


        transactionRecordHashMapTarget.put("TR-478842222022245",
                new TransactionRecord.Builder("TR-478842222022245", new BigDecimal("170"), "USD"
                        , DateUtility.parseDate("29/01/2020", datePattern))
                        .setPurpose("donation")
                        .build());

        transactionRecordHashMapTarget.put("TR-47884222202",
                new TransactionRecord.Builder("TR-47884222202", new BigDecimal("80"), "JOD"
                        , DateUtility.parseDate("22/01/2020", datePattern))
                        .setPurpose("donation")
                        .build());

        transactionRecordHashMapTarget.put("TR-47884222203",
                new TransactionRecord.Builder("TR-47884222203", new BigDecimal("5000"), "USD"
                        , DateUtility.parseDate("27/01/2020", datePattern))
                        .setPurpose("not specified")
                        .build());
    }
}
