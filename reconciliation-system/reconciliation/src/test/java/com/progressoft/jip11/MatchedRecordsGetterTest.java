package com.progressoft.jip11;

import com.progressoft.jip11.entity.FileRecord;
import com.progressoft.jip11.entity.TransactionRecord;
import com.progressoft.jip11.exceptions.EmptyTransactionsException;
import com.progressoft.jip11.recordmatcher.MatchedRecordsHandler;
import com.progressoft.jip11.recordmatcher.matchingimpl.MatchedRecordsGetter;
import com.progressoft.jip11.utility.DateUtility;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;

public class MatchedRecordsGetterTest {
    MatchedRecordsHandler matchedRecords;

    @BeforeEach
    public void setup() {
        matchedRecords = new MatchedRecordsGetter();
        CurrencyStore.getCurrencyStore().addToCurrencySpecification("USD" , 2);
        CurrencyStore.getCurrencyStore().addToCurrencySpecification("JOD" , 3);
    }

    @Test
    public void canCreate() {
        new MatchedRecordsGetter();
    }

    @Test
    public void givenNullHashMaps_whenGetMatchedRecords_throwNullPointerException() {
        NullPointerException nullPointerException = Assertions.assertThrows(NullPointerException.class,
                () -> matchedRecords.getMatchedRecords(null, null));
        Assertions.assertEquals(nullPointerException.getMessage() , "Null transactions");
    }

    @Test
    public void givenEmptyHashMap_whenGetMatchedRecords_throwEmptyTransactionsException() {
        EmptyTransactionsException emptyTransactionsException = Assertions.assertThrows(EmptyTransactionsException.class,
                () -> matchedRecords.getMatchedRecords(new LinkedHashMap<>(), new LinkedHashMap<>()));
        Assertions.assertEquals(emptyTransactionsException.getMessage() , "Empty transactions");
    }

    @Test
    public void givenValidData_whenGetMatchedRecords_thenResultIsReturned(){
        HashMap<String , TransactionRecord> transactionRecordHashMapSource = new LinkedHashMap<>();
        HashMap<String , TransactionRecord> transactionRecordHashMapTarget = new LinkedHashMap<>();

        generateData(transactionRecordHashMapSource , transactionRecordHashMapTarget);

        ArrayList<FileRecord> fileRecords = matchedRecords.getMatchedRecords(transactionRecordHashMapSource , transactionRecordHashMapTarget);
        String datePattern = "dd/MM/yyyy";


        Assertions.assertNotNull(fileRecords);
        Assertions.assertEquals(fileRecords.size() , 2);
        Assertions.assertNotNull(fileRecords.get(0));
        Assertions.assertEquals(fileRecords.get(0).getId(),"TR-47884222202");
        Assertions.assertEquals(fileRecords.get(0).getAmount(),new BigDecimal("30.000"));
        Assertions.assertEquals(fileRecords.get(0).getDate(),DateUtility.parseDate("22/01/2020", datePattern));
        Assertions.assertEquals(fileRecords.get(0).getCurrency(),"JOD");
        Assertions.assertEquals(fileRecords.get(0).getFileTag(),"SOURCEANDTARGET");

        Assertions.assertNotNull(fileRecords.get(1));
        Assertions.assertEquals(fileRecords.get(1).getId(),"TR-47884222203");
        Assertions.assertEquals(fileRecords.get(1).getAmount(),new BigDecimal("5000.000"));
        Assertions.assertEquals(fileRecords.get(1).getDate(),DateUtility.parseDate("25/01/2020", datePattern));
        Assertions.assertEquals(fileRecords.get(1).getCurrency(),"JOD");
        Assertions.assertEquals(fileRecords.get(1).getFileTag(),"SOURCEANDTARGET");
    }

    private void generateData(HashMap<String, TransactionRecord> transactionRecordHashMapSource, HashMap<String, TransactionRecord> transactionRecordHashMapTarget) {
        String datePattern = "dd/MM/yyyy";

        transactionRecordHashMapSource.put("TR-47884222201",
                new TransactionRecord.Builder("TR-47884222201", new BigDecimal("140"), "USD"
                        , DateUtility.parseDate("20/01/2020", datePattern))
                        .setPurpose("donation")
                        .build());

        transactionRecordHashMapSource.put("TR-47884222202",
                new TransactionRecord.Builder("TR-47884222202", new BigDecimal("30"), "JOD"
                        , DateUtility.parseDate("2020-01-22", "yyyy-MM-dd"))
                        .setPurpose("donation")
                        .build());

        transactionRecordHashMapSource.put("TR-47884222203",
                new TransactionRecord.Builder("TR-47884222203", new BigDecimal("5000"), "JOD"
                        , DateUtility.parseDate("25/01/2020", datePattern))
                        .setPurpose("not specified")
                        .build());


        transactionRecordHashMapTarget.put("TR-478842222022245",
                new TransactionRecord.Builder("TR-478842222022245", new BigDecimal("170"), "USD"
                        , DateUtility.parseDate("20/01/2020", datePattern))
                        .setPurpose("donation")
                        .build());

        transactionRecordHashMapTarget.put("TR-47884222202",
                new TransactionRecord.Builder("TR-47884222202", new BigDecimal("30"), "JOD"
                        , DateUtility.parseDate("22/01/2020", datePattern))
                        .setPurpose("donation")
                        .build());

        transactionRecordHashMapTarget.put("TR-47884222203",
                new TransactionRecord.Builder("TR-47884222203", new BigDecimal("5000"), "JOD"
                        , DateUtility.parseDate("25/01/2020", datePattern))
                        .setPurpose("not specified")
                        .build());
    }
}
