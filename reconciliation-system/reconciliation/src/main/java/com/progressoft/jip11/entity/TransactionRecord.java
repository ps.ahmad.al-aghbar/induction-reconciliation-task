package com.progressoft.jip11.entity;

import com.progressoft.jip11.CurrencyStore;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Date;
import java.util.Objects;

public class TransactionRecord {
    private final String id;
    private final BigDecimal amount;
    private final String currency;
    private final LocalDate date;
    private final String purpose;
    private final String type;
    private final String description;

    public TransactionRecord(Builder builder) {
        this.id = builder.ID;
        this.description = builder.description;
        this.amount = builder.amount;
        this.currency = builder.currency;
        this.purpose = builder.purpose;
        this.date = builder.date;
        this.type = builder.type;
    }

    public String getId() {
        return id;
    }

    public String getDescription() {
        return description;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public String getCurrency() {
        return currency;
    }

    public String getPurpose() {
        return purpose;
    }

    public LocalDate getDate() {
        return date;
    }

    public String getType() {
        return type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TransactionRecord record = (TransactionRecord) o;
        return Objects.equals(id, record.id) &&
                Objects.equals(currency, record.currency) &&
                amount.equals(record.amount.setScale(CurrencyStore.getCurrencyStore().getSpecification(currency))) &&
                Objects.equals(date, record.date);
    }

    int hashCache = 0;

    @Override
    public int hashCode() {
        return hashCache == 0 ? (hashCache = Objects.hash(id, currency, amount, date)) : hashCache;
    }


    public static class Builder {
        private final String ID;
        private final BigDecimal amount;
        private final String currency;
        private final LocalDate date;
        private String purpose;
        private String description;
        private String type;

        public Builder(String ID, BigDecimal amount, String currency, LocalDate date) {
            this.ID = ID;
            this.amount = amount.setScale(CurrencyStore.getCurrencyStore().getSpecification(currency));
            this.currency = currency;
            this.date = date;
        }

        public Builder setDescription(String description) {
            this.description = description;
            return this;
        }

        public Builder setPurpose(String purpose) {
            this.purpose = purpose;
            return this;
        }

        public Builder setType(String type) {
            this.type = type;
            return this;
        }

        public TransactionRecord build() {
            return new TransactionRecord(this);
        }
    }
}

