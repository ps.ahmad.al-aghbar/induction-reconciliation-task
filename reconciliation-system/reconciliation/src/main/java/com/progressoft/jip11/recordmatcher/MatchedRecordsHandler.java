package com.progressoft.jip11.recordmatcher;

import com.progressoft.jip11.entity.FileRecord;
import com.progressoft.jip11.entity.TransactionRecord;

import java.util.ArrayList;
import java.util.HashMap;

public interface MatchedRecordsHandler {
    ArrayList<FileRecord> getMatchedRecords(HashMap<String, TransactionRecord> sourceTransactions, HashMap<String, TransactionRecord> targetTransactions);
}
