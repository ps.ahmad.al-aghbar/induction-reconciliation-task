package com.progressoft.jip11;

import java.util.HashMap;

public class CurrencyStore {
    private static final CurrencyStore currencyStore = new CurrencyStore();
    private final HashMap<String, Integer> currenciesWithSpecifications = new HashMap<>();

    private CurrencyStore() {

    }

    public static CurrencyStore getCurrencyStore() {
        return currencyStore;
    }

    public void addToCurrencySpecification(String currency, int decimalLength) {
        currenciesWithSpecifications.put(currency, decimalLength);
    }

    public int getSpecification(String currency) {
        return currenciesWithSpecifications.get(currency);
    }
}
