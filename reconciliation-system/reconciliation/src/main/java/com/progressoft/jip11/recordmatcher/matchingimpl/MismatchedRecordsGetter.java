package com.progressoft.jip11.recordmatcher.matchingimpl;

import com.progressoft.jip11.entity.FileRecord;
import com.progressoft.jip11.entity.TransactionRecord;
import com.progressoft.jip11.exceptions.EmptyTransactionsException;
import com.progressoft.jip11.recordmatcher.MismatchedRecordsHandler;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;

public class MismatchedRecordsGetter implements MismatchedRecordsHandler {
    @Override
    public ArrayList<FileRecord> getMismatchedRecords(HashMap<String, TransactionRecord> sourceTransactions, HashMap<String, TransactionRecord> targetTransactions) {
        if (sourceTransactions == null || targetTransactions == null)
            throw new NullPointerException("Null transactions");
        if (sourceTransactions.isEmpty() || targetTransactions.isEmpty())
            throw new EmptyTransactionsException("Empty transactions");

        ArrayList<FileRecord> fileRecords = new ArrayList<>();

        for (String id : sourceTransactions.keySet()) {
            if (targetTransactions.containsKey(id)) {
                if (!sourceTransactions.get(id).equals(targetTransactions.get(id))) {
                    fileRecords.add(mapToFileRecord(sourceTransactions.get(id), "SOURCE"));
                    fileRecords.add(mapToFileRecord(targetTransactions.get(id), "TARGET"));
                }
            }
        }

        return fileRecords;
    }

    public FileRecord mapToFileRecord(TransactionRecord transactionRecord, String fileTag) {
        return new FileRecord.Builder(transactionRecord.getId(), transactionRecord.getAmount(),
                transactionRecord.getCurrency(), transactionRecord.getDate())
                .setFileTag(fileTag)
                .build();
    }
}
