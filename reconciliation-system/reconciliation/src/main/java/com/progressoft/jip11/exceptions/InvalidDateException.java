package com.progressoft.jip11.exceptions;

import java.text.ParseException;

public class InvalidDateException extends RuntimeException {

    public InvalidDateException(String message, Exception exception) {
        super(message, exception);
    }

    public InvalidDateException(String message) {
        super(message);
    }
}
