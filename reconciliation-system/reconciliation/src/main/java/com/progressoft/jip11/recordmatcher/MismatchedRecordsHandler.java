package com.progressoft.jip11.recordmatcher;

import com.progressoft.jip11.entity.FileRecord;
import com.progressoft.jip11.entity.TransactionRecord;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;

public interface MismatchedRecordsHandler {
    ArrayList<FileRecord> getMismatchedRecords(HashMap<String, TransactionRecord> sourceTransactions, HashMap<String, TransactionRecord> targetTransactions);
}
