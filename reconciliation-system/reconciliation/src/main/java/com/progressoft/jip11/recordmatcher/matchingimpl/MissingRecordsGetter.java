package com.progressoft.jip11.recordmatcher.matchingimpl;

import com.progressoft.jip11.entity.FileRecord;
import com.progressoft.jip11.entity.TransactionRecord;
import com.progressoft.jip11.exceptions.EmptyTransactionsException;
import com.progressoft.jip11.recordmatcher.MissingRecordsHandler;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;

public class MissingRecordsGetter implements MissingRecordsHandler {
    @Override
    public ArrayList<FileRecord> getMissingRecords(HashMap<String, TransactionRecord> sourceTransaction, HashMap<String, TransactionRecord> targetTransaction) {
        if (sourceTransaction == null || targetTransaction == null)
            throw new NullPointerException("Null transactions");
        if (sourceTransaction.isEmpty() || targetTransaction.isEmpty())
            throw new EmptyTransactionsException("Empty transactions");

        ArrayList<FileRecord> fileRecords = new ArrayList<>();

        for (String id : sourceTransaction.keySet()) {
            if (!targetTransaction.containsKey(id)) {
                fileRecords.add(mapToFileRecord(sourceTransaction.get(id), "SOURCE"));
            }
        }
        for (String id : targetTransaction.keySet()) {
            if (!sourceTransaction.containsKey(id)) {
                fileRecords.add(mapToFileRecord(targetTransaction.get(id), "TARGET"));
            }
        }
        return fileRecords;
    }

    public FileRecord mapToFileRecord(TransactionRecord transactionRecord, String fileTag) {
        return new FileRecord.Builder(transactionRecord.getId(), transactionRecord.getAmount(),
                transactionRecord.getCurrency(), transactionRecord.getDate())
                .setFileTag(fileTag)
                .build();
    }
}
