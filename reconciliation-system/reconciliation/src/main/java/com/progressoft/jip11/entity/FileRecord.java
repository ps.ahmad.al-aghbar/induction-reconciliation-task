package com.progressoft.jip11.entity;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Date;

public class FileRecord {
    private final String id;
    private final BigDecimal amount;
    private final String currency;
    private final LocalDate date;
    private final String fileTag;

    public FileRecord(Builder builder) {
        this.id = builder.ID;
        this.amount = builder.amount;
        this.currency = builder.currency;
        this.date = builder.date;
        this.fileTag = builder.fileTag;
    }

    public String getId() {
        return id;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public String getCurrency() {
        return currency;
    }

    public LocalDate getDate() {
        return date;
    }

    public String getFileTag() {
        return fileTag;
    }

    public static class Builder {
        private final String ID;
        private final BigDecimal amount;
        private final String currency;
        private final LocalDate date;
        private String fileTag;

        public Builder(String ID, BigDecimal amount, String currency, LocalDate date) {
            this.ID = ID;
            this.amount = amount;
            this.currency = currency;
            this.date = date;
        }


        public FileRecord.Builder setFileTag(String fileTag) {
            this.fileTag = fileTag;
            return this;
        }

        public FileRecord build() {
            return new FileRecord(this);
        }
    }
}
