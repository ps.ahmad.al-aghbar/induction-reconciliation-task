package com.progressoft.jip11.exceptions;

public class EmptyTransactionsException extends RuntimeException {
    public EmptyTransactionsException(String message) {
        super(message);
    }
}
