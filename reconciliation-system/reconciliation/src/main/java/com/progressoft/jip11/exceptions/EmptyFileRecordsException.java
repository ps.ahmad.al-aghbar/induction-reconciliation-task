package com.progressoft.jip11.exceptions;

public class EmptyFileRecordsException extends RuntimeException{
    public EmptyFileRecordsException(String message) {
        super(message);
    }
}
