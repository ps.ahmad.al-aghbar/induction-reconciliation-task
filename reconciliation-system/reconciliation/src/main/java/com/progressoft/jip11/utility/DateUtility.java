package com.progressoft.jip11.utility;

import com.progressoft.jip11.exceptions.InvalidDateException;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.util.Date;

public class DateUtility {

    public static LocalDate parseDate(String s, String datePattern) {
        DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern(datePattern);
        LocalDate date;
        date = LocalDate.parse(s, dateFormat);
        return date;
    }
}
