package com.progressoft.jip11.recordmatcher;

import com.progressoft.jip11.entity.FileRecord;
import com.progressoft.jip11.entity.TransactionRecord;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;

public interface MissingRecordsHandler {
    ArrayList<FileRecord> getMissingRecords(HashMap<String, TransactionRecord> sourceTransaction, HashMap<String, TransactionRecord> targetTransaction);
}
