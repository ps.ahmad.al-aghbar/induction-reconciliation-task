import com.progressoft.jip11.CurrencyStore;
import com.progressoft.jip11.filecommuniacationapp.FileReconciliationRequest;
import com.progressoft.jip11.filecommuniacationapp.ReconciliationFiles;
import com.progressoft.jip11.mapper.mapperimpl.CSVDefaultMapper;
import com.progressoft.jip11.mapper.mapperimpl.JSONDefaultMapper;
import com.progressoft.jip11.reader.readerimpl.CSVReader;
import com.progressoft.jip11.reader.readerimpl.JSONReader;
import com.progressoft.jip11.writer.writeimpl.CSVWriter;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class ReconciliationFilesTest {
    ReconciliationFiles reconciliationFiles;

    @Test
    public void canCreate() {
        new ReconciliationFiles();
    }

    @BeforeEach
    public void setup() {
        reconciliationFiles = new ReconciliationFiles();
        CurrencyStore.getCurrencyStore().addToCurrencySpecification("USD", 2);
        CurrencyStore.getCurrencyStore().addToCurrencySpecification("JOD", 3);
    }

    @Test
    public void givenNullSourceReader_whenCreateNewFileReconciliationRequest_thenThrowNullPointerException() {
        Assertions.assertThrows(NullPointerException.class,
                () -> new FileReconciliationRequest.Builder(null, new JSONReader(new JSONDefaultMapper()),
                        new CSVWriter(), Path.of("."), Path.of(".")).build());
    }

    @Test
    public void givenNullTargetReader_whenCreateNewFileReconciliationRequest_thenThrowNullPointerException() {
        Assertions.assertThrows(NullPointerException.class,
                () -> new FileReconciliationRequest.Builder(new CSVReader(new CSVDefaultMapper()), null,
                        new CSVWriter(), Path.of("."), Path.of(".")).build());
    }

    @Test
    public void givenNullFileWriter_whenCreateNewFileReconciliationRequest_thenThrowNullPointerException() {
        Assertions.assertThrows(NullPointerException.class,
                () -> new FileReconciliationRequest.Builder(new CSVReader(new CSVDefaultMapper()), new JSONReader(new JSONDefaultMapper()),
                        null, Path.of("."), Path.of(".")).build());
    }

    @Test
    public void givenNullSourcePath_whenCreateNewFileReconciliationRequest_thenThrowNullPointerException() {
        Assertions.assertThrows(NullPointerException.class,
                () -> new FileReconciliationRequest.Builder(new CSVReader(new CSVDefaultMapper()), new JSONReader(new JSONDefaultMapper()),
                        new CSVWriter(), null, Path.of(".")).build());
    }

    @Test
    public void givenNullTargetPath_whenCreateNewFileReconciliationRequest_thenThrowNullPointerException() {
        Assertions.assertThrows(NullPointerException.class,
                () -> new FileReconciliationRequest.Builder(new CSVReader(new CSVDefaultMapper()), new JSONReader(new JSONDefaultMapper()),
                        new CSVWriter(), Path.of("."), null).build());
    }

    @Test
    public void givenCorrectParameters_whenProcessReconciliation_thenThreeFilesCreatedWithData() throws IOException {
        Path sourcePath = Files.createTempFile("reconciliation-bank-transactions", ".csv");
        copyOriginalFileToTempFile(sourcePath, "/bank-transactions.csv");
        Path targetPath = Files.createTempFile("reconciliation-online-banking-transactions", ".json");
        copyOriginalFileToTempFile(targetPath, "/online-banking-transactions.json");
        FileReconciliationRequest reconciliationRequest = new FileReconciliationRequest.Builder(new CSVReader(new CSVDefaultMapper()), new JSONReader(new JSONDefaultMapper()), new CSVWriter(), sourcePath, targetPath).build();


        reconciliationFiles.processReconciliation(reconciliationRequest);


        Path mismatchingDesktopPath = Path.of(System.getProperty("user.home"), "Desktop", "Reconciliation", "Mismatching Transactions file.csv");
        Path matchingDesktopPath = Path.of(System.getProperty("user.home"), "Desktop", "Reconciliation", "Matching Transactions file.csv");
        Path missingDesktopPath = Path.of(System.getProperty("user.home"), "Desktop", "Reconciliation", "Missing Transactions file.csv");


        Assertions.assertTrue(Files.exists(mismatchingDesktopPath));
        Assertions.assertTrue(Files.exists(matchingDesktopPath));
        Assertions.assertTrue(Files.exists(missingDesktopPath));


        Assertions.assertTrue(Files.size(mismatchingDesktopPath) > 0);
        Assertions.assertTrue(Files.size(matchingDesktopPath) > 0);
        Assertions.assertTrue(Files.size(missingDesktopPath) > 0);


        Path matchingTestPath = Files.createTempFile("reconciliation-matching-bank-transactions", ".csv");
        copyOriginalFileToTempFile(matchingTestPath, "/matching-bank-transactions.csv");
        Path mismatchingTestPath = Files.createTempFile("reconciliation-mismatching-bank-transactions", ".csv");
        copyOriginalFileToTempFile(mismatchingTestPath, "/mismatching-bank-transactions.csv");
        Path missingTestPath = Files.createTempFile("reconciliation-missing-bank-transactions", ".csv");
        copyOriginalFileToTempFile(missingTestPath, "/missing-bank-transactions.csv");


        Assertions.assertEquals(Files.readAllLines(mismatchingTestPath), Files.readAllLines(mismatchingDesktopPath));
        Assertions.assertEquals(Files.readAllLines(matchingTestPath), Files.readAllLines(matchingDesktopPath));
        Assertions.assertEquals(Files.readAllLines(missingTestPath), Files.readAllLines(missingDesktopPath));
    }

    private void copyOriginalFileToTempFile(Path path, String originalFile) throws IOException {
        try (InputStream resourceAsStream = this.getClass().getResourceAsStream(originalFile);
             OutputStream outputStream = Files.newOutputStream(path)) {
            int read;
            while ((read = resourceAsStream.read()) != -1)
                outputStream.write(read);
        }
    }
}
