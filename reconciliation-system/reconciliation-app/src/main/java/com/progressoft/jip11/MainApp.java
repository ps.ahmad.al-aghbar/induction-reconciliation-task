package com.progressoft.jip11;


import com.progressoft.jip11.filecommuniacationapp.FileReconciliationRequest;
import com.progressoft.jip11.filecommuniacationapp.ReconciliationFiles;
import com.progressoft.jip11.mapper.mapperimpl.CSVDefaultMapper;
import com.progressoft.jip11.mapper.mapperimpl.JSONDefaultMapper;
import com.progressoft.jip11.reader.FileReader;
import com.progressoft.jip11.reader.readerimpl.CSVReader;
import com.progressoft.jip11.reader.readerimpl.JSONReader;
import com.progressoft.jip11.writer.FileWriter;
import com.progressoft.jip11.writer.writeimpl.CSVWriter;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Locale;
import java.util.Scanner;

import static com.progressoft.jip11.CurrencyStore.*;

public class MainApp {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        FileReader source;
        FileReader target;
        ReconciliationFiles reconciliationFiles = new ReconciliationFiles();
        getCurrencyStore().addToCurrencySpecification("USD", 2);
        getCurrencyStore().addToCurrencySpecification("JOD", 3);

        FileWriter fileWriter = new CSVWriter();

        System.out.println("Please enter source path");
        Path sourcePath = Paths.get(scanner.nextLine());

        System.out.println("Please enter source file format:");
        String sourceFormat = scanner.nextLine();

        System.out.println("Please enter target path");
        Path targetPath = Paths.get(scanner.nextLine());


        System.out.println("Please enter target file format:");
        String targetFormat = scanner.nextLine();

        source = checkFormat(sourceFormat);
        target = checkFormat(targetFormat);


        FileReconciliationRequest reconciliationRequest = new FileReconciliationRequest.Builder(source, target, fileWriter, sourcePath, targetPath).build();
        System.out.println(reconciliationFiles.processReconciliation(reconciliationRequest));


    }

    private static FileReader checkFormat(String targetFormat) {
        switch (targetFormat.toUpperCase(Locale.ROOT)) {
            case "CSV":
                return new CSVReader(new CSVDefaultMapper());
            case "JSON":
                return new JSONReader(new JSONDefaultMapper());
            default:
                throw new IllegalStateException("Source format is wrong");
        }
    }
}
