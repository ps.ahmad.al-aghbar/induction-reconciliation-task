package com.progressoft.jip11.filecommuniacationapp;

import com.progressoft.jip11.entity.FileRecord;
import com.progressoft.jip11.reader.FileReader;
import com.progressoft.jip11.writer.FileWriter;

import java.math.BigDecimal;
import java.nio.file.Path;
import java.time.LocalDate;

public class FileReconciliationRequest {
    private final FileReader sourceReader;
    private final FileReader targetReader;
    private final FileWriter fileWriter;
    private final Path sourcePath;
    private final Path targetPath;

    public FileReconciliationRequest(Builder builder) {
        this.sourceReader = builder.sourceReader;
        this.targetReader = builder.targetReader;
        this.fileWriter = builder.fileWriter;
        this.sourcePath = builder.sourcePath;
        this.targetPath = builder.targetPath;
    }

    public FileReader getSourceReader() {
        return sourceReader;
    }

    public FileReader getTargetReader() {
        return targetReader;
    }

    public FileWriter getFileWriter() {
        return fileWriter;
    }

    public Path getSourcePath() {
        return sourcePath;
    }

    public Path getTargetPath() {
        return targetPath;
    }

    public static class Builder {
        private final FileReader sourceReader;
        private final FileReader targetReader;
        private final FileWriter fileWriter;
        private final Path sourcePath;
        private final Path targetPath;

        public Builder(FileReader sourceReader, FileReader targetReader, FileWriter fileWriter, Path sourcePath, Path targetPath) {
            this.sourceReader = sourceReader;
            this.targetReader = targetReader;
            this.fileWriter = fileWriter;
            this.sourcePath = sourcePath;
            this.targetPath = targetPath;
        }

        private void validate() {
            if (targetReader == null) {
                throw new NullPointerException("Null target reader");
            } else if (sourceReader == null) {
                throw new NullPointerException("Null source reader");
            } else if (fileWriter == null) {
                throw new NullPointerException("Null file writer");
            } else if (sourcePath == null) {
                throw new NullPointerException("Null source path");
            } else if (targetPath == null) {
                throw new NullPointerException("Null target path");
            }
        }

        public FileReconciliationRequest build() {
            validate();
            return new FileReconciliationRequest(this);
        }
    }
}
