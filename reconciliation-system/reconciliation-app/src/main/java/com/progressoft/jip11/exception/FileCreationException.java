package com.progressoft.jip11.exception;

import java.io.IOException;

public class FileCreationException extends RuntimeException{

    public FileCreationException(String message, Exception exception) {
        super(message , exception);
    }
}
