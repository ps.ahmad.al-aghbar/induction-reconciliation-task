package com.progressoft.jip11.filecommuniacationapp;

import com.progressoft.jip11.entity.FileRecord;
import com.progressoft.jip11.entity.TransactionRecord;
import com.progressoft.jip11.exception.FileCreationException;
import com.progressoft.jip11.fileheader.fileheaderimpl.FileHeaderWithFoundFile;
import com.progressoft.jip11.fileheader.fileheaderimpl.FileHeaderWithoutFoundFile;
import com.progressoft.jip11.recordmatcher.MatchedRecordsHandler;
import com.progressoft.jip11.recordmatcher.MismatchedRecordsHandler;
import com.progressoft.jip11.recordmatcher.MissingRecordsHandler;
import com.progressoft.jip11.recordmatcher.matchingimpl.MatchedRecordsGetter;
import com.progressoft.jip11.recordmatcher.matchingimpl.MismatchedRecordsGetter;
import com.progressoft.jip11.recordmatcher.matchingimpl.MissingRecordsGetter;
import com.progressoft.jip11.writer.FileWriter;
import com.progressoft.jip11.writer.writeimpl.WriteRequest;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;

public class ReconciliationFiles {
    public String processReconciliation(FileReconciliationRequest fileReconciliationRequest) {
        MatchedRecordsHandler matchedRecords = new MatchedRecordsGetter();
        MissingRecordsHandler missingRecords = new MissingRecordsGetter();
        MismatchedRecordsHandler mismatchedRecords = new MismatchedRecordsGetter();


        HashMap<String, TransactionRecord> sourceRecordsMap = fileReconciliationRequest.getSourceReader().read(fileReconciliationRequest.getSourcePath());
        HashMap<String, TransactionRecord> targetRecordsMap = fileReconciliationRequest.getTargetReader().read(fileReconciliationRequest.getTargetPath());

        ArrayList<FileRecord> matchedRecordsList = matchedRecords.getMatchedRecords(sourceRecordsMap, targetRecordsMap);
        ArrayList<FileRecord> mismatchedRecordsList = mismatchedRecords.getMismatchedRecords(sourceRecordsMap, targetRecordsMap);
        ArrayList<FileRecord> missingRecordsList = missingRecords.getMissingRecords(sourceRecordsMap, targetRecordsMap);

        try {
            Files.createDirectories(Path.of(System.getProperty("user.home"), "Desktop", "Reconciliation"));
        } catch (IOException exception) {
            throw new FileCreationException("Error  in created directory in desktop ", exception);
        }


        Path desktopPath;

        desktopPath = Path.of(System.getProperty("user.home"), "Desktop", "Reconciliation", "Mismatching Transactions file.csv");
        fileReconciliationRequest.getFileWriter().write(new WriteRequest(desktopPath, mismatchedRecordsList, new FileHeaderWithFoundFile()));

        desktopPath = Path.of(System.getProperty("user.home"), "Desktop", "Reconciliation", "Missing Transactions file.csv");

        fileReconciliationRequest.getFileWriter().write(new WriteRequest(desktopPath, missingRecordsList, new FileHeaderWithFoundFile()));

        desktopPath = Path.of(System.getProperty("user.home"), "Desktop", "Reconciliation", "Matching Transactions file.csv");
        fileReconciliationRequest.getFileWriter().write(new WriteRequest(desktopPath, matchedRecordsList, new FileHeaderWithoutFoundFile()));

        return "Reconciliation finished.\n" +
                "Result files are available in directory " + System.getProperty("user.home") + "/Desktop" + "/Reconciliation";
    }

}
