package com.progressoft.jip11;

import com.progressoft.jip11.entity.TransactionRecord;
import com.progressoft.jip11.exceptions.DuplicateIDException;
import com.progressoft.jip11.exceptions.EmptyIteratorException;
import com.progressoft.jip11.exceptions.InvalidColumnsInFileException;
import com.progressoft.jip11.exceptions.InvalidDateException;
import com.progressoft.jip11.mapper.RecordMapper;
import com.progressoft.jip11.mapper.mapperimpl.CSVDefaultMapper;
import org.apache.commons.collections.iterators.ArrayIterator;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;

public class CSVDefaultMapperTest {
    RecordMapper<String[]> csvDefaultMapper;

    @BeforeEach
    public void setup(){
        csvDefaultMapper = new CSVDefaultMapper();
    }

    @Test
    public void canCreate() {
        new CSVDefaultMapper();
    }

    @Test
    public void givenNullTransactions_whenMap_throwNullPointerException() {
        NullPointerException nullPointerException = Assertions.assertThrows(NullPointerException.class, () -> csvDefaultMapper.map(null));
        Assertions.assertEquals(nullPointerException.getMessage(), "Null transactions");
    }

    @Test
    public void givenEmptyTransactions_whenMap_thenThrowEmptyIteratorException() {

        EmptyIteratorException emptyIteratorException = Assertions.assertThrows(EmptyIteratorException.class, () -> csvDefaultMapper.map(new ArrayIterator()));

        Assertions.assertEquals(emptyIteratorException.getMessage(), "Empty iterator");
    }

    @Test
    public void givenInvalidTransactionsColumnsLength_whenMap_thenThrowEmptyIteratorException() {
        ArrayList<String[]> arrayLists = new ArrayList<>();
        String[] record = new String[]{"1111", "online transfer", "140", "USD", "donation", "2020-01-20"};

        arrayLists.add(record);

        InvalidColumnsInFileException invalidColumnsInFileException = Assertions.assertThrows(InvalidColumnsInFileException.class, () -> csvDefaultMapper.map(arrayLists.iterator()));

        Assertions.assertEquals(invalidColumnsInFileException.getMessage(), "Wrong columns");
    }

    @Test
    public void givenDuplicateIDS_whenMap_thenThrowDuplicateIDException() {
        ArrayList<String[]> arrayLists = new ArrayList<>();
        String[] record1 = new String[]{"1111", "online transfer", "140", "USD", "donation", "2020-01-20", "D"};
        String[] record2 = new String[]{"1111", "online transfer", "140", "USD", "donation", "2020-01-20", "D"};

        arrayLists.add(record1);
        arrayLists.add(record2);

        DuplicateIDException duplicateIDException = Assertions.assertThrows(DuplicateIDException.class, () -> csvDefaultMapper.map(arrayLists.iterator()));

        Assertions.assertEquals(duplicateIDException.getMessage(), "Duplicate IDs found");
    }

    @Test
    public void givenInvalidDate_whenMap_thenThrowInvalidDateException() {
        ArrayList<String[]> arrayLists = new ArrayList<>();
        String[] record1 = new String[]{"1111", "online transfer", "140", "USD", "donation", "20-01-2010", "D"};
        String[] record2 = new String[]{"1111", "online transfer", "140", "USD", "donation", "2020-01-20", "D"};

        arrayLists.add(record1);
        arrayLists.add(record2);

        InvalidDateException invalidDateException = Assertions.assertThrows(InvalidDateException.class, () -> csvDefaultMapper.map(arrayLists.iterator()));

        Assertions.assertEquals(invalidDateException.getMessage(), "Wrong date format");
    }

    @Test
    public void givenValidTransactions_whenMap_thenResultIsReturned() {
        ArrayList<String[]> arrayLists = new ArrayList<>();


        CurrencyStore.getCurrencyStore().addToCurrencySpecification("USD" , 2);
        CurrencyStore.getCurrencyStore().addToCurrencySpecification("JOD" , 3);
        generateIteratorData(arrayLists);

        HashMap<String, TransactionRecord> transactionRecordHashMap = csvDefaultMapper.map(arrayLists.iterator());

        Assertions.assertNotNull(transactionRecordHashMap);
        Assertions.assertEquals(transactionRecordHashMap.size(), 3);

        Assertions.assertNotNull(transactionRecordHashMap.get("1111"));
        Assertions.assertEquals(transactionRecordHashMap.get("1111").getId(), "1111");
        Assertions.assertEquals(transactionRecordHashMap.get("1111").getType(), "D");
        Assertions.assertEquals(transactionRecordHashMap.get("1111").getPurpose(), "donation");
        Assertions.assertEquals(transactionRecordHashMap.get("1111").getCurrency(), "USD");
        Assertions.assertEquals(transactionRecordHashMap.get("1111").getAmount(), new BigDecimal("140.00"));
        Assertions.assertEquals(transactionRecordHashMap.get("1111").getDescription(), "online transfer");
        Assertions.assertEquals(transactionRecordHashMap.get("1111").getDate(), LocalDate.parse("2020-01-20"));


        Assertions.assertNotNull(transactionRecordHashMap.get("2222"));
        Assertions.assertEquals(transactionRecordHashMap.get("2222").getId(), "2222");
        Assertions.assertEquals(transactionRecordHashMap.get("2222").getType(), "D");
        Assertions.assertEquals(transactionRecordHashMap.get("2222").getPurpose(), "donation");
        Assertions.assertEquals(transactionRecordHashMap.get("2222").getCurrency(), "USD");
        Assertions.assertEquals(transactionRecordHashMap.get("2222").getAmount(), new BigDecimal("150.00"));
        Assertions.assertEquals(transactionRecordHashMap.get("2222").getDescription(), "online transfer");
        Assertions.assertEquals(transactionRecordHashMap.get("2222").getDate(), LocalDate.parse("2020-11-20"));


        Assertions.assertNotNull(transactionRecordHashMap.get("3333"));
        Assertions.assertEquals(transactionRecordHashMap.get("3333").getId(), "3333");
        Assertions.assertEquals(transactionRecordHashMap.get("3333").getType(), "D");
        Assertions.assertEquals(transactionRecordHashMap.get("3333").getPurpose(), "donation");
        Assertions.assertEquals(transactionRecordHashMap.get("3333").getCurrency(), "USD");
        Assertions.assertEquals(transactionRecordHashMap.get("3333").getAmount(), new BigDecimal("200.00"));
        Assertions.assertEquals(transactionRecordHashMap.get("3333").getDescription(), "atm withdrwal");
        Assertions.assertEquals(transactionRecordHashMap.get("3333").getDate(), LocalDate.parse("2020-05-20"));
    }

    private void generateIteratorData(ArrayList<String[]> arrayLists) {
        String[] record1 = new String[]{"1111", "online transfer", "140", "USD", "donation", "2020-01-20", "D"};
        String[] record2 = new String[]{"2222", "online transfer", "150", "USD", "donation", "2020-11-20", "D"};
        String[] record3 = new String[]{"3333", "atm withdrwal", "200", "USD", "donation", "2020-05-20", "D"};

        arrayLists.add(record1);
        arrayLists.add(record2);
        arrayLists.add(record3);
    }
}
