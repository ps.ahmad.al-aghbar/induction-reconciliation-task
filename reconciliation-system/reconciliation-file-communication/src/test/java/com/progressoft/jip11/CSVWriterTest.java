package com.progressoft.jip11;

import com.progressoft.jip11.entity.FileRecord;
import com.progressoft.jip11.exceptions.EmptyFileRecordsException;
import com.progressoft.jip11.fileheader.fileheaderimpl.FileHeaderWithFoundFile;
import com.progressoft.jip11.fileheader.fileheaderimpl.FileHeaderWithoutFoundFile;
import com.progressoft.jip11.utility.DateUtility;
import com.progressoft.jip11.writer.writeimpl.CSVWriter;
import com.progressoft.jip11.writer.writeimpl.WriteRequest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;

public class CSVWriterTest {

    @Test
    public void canCreate() {
        new CSVWriter();
    }

    @Test
    public void givenNullPathAndNullArrayList_whenWrite_throwNullPointerException() {
        CSVWriter csvWriter = new CSVWriter();

        NullPointerException nullPointerException = Assertions.assertThrows(NullPointerException.class, () -> csvWriter.write(new WriteRequest(null, new ArrayList<>(), null)));

        Assertions.assertEquals(nullPointerException.getMessage(), "Null path");

        nullPointerException = Assertions.assertThrows(NullPointerException.class, () -> csvWriter.write(new WriteRequest(Path.of("."), null, null)));

        Assertions.assertEquals(nullPointerException.getMessage(), "Null file records");
    }

    @Test
    public void givenValidPath_whenWrite_throwEmptyFileRecordsException() throws IOException {
        CSVWriter csvWriter = new CSVWriter();
        ArrayList<FileRecord> fileRecords = new ArrayList<>();
        Path path = Files.createTempFile("reconciliation-temp-write", ".csv");
        generateData(fileRecords);
        WriteRequest writeRequest = new WriteRequest(path, fileRecords, new FileHeaderWithoutFoundFile());
        csvWriter.write(writeRequest);

        Assertions.assertTrue(Files.exists(path));
        Assertions.assertTrue(Files.size(path) > 0);
    }

    private void generateData(ArrayList<FileRecord> fileRecords) {
        fileRecords.add(new FileRecord.Builder("1111", new BigDecimal("20.000"), "JOD", DateUtility.parseDate("25/01/2020", "dd/MM/yyyy")).build());
        fileRecords.add(new FileRecord.Builder("1111", new BigDecimal("20"), "JOD", DateUtility.parseDate("25/01/2020", "dd/MM/yyyy")).build());
        fileRecords.add(new FileRecord.Builder("1111", new BigDecimal("20"), "JOD", DateUtility.parseDate("25/01/2020", "dd/MM/yyyy")).build());
    }

}
