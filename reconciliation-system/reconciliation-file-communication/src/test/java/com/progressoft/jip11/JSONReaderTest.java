package com.progressoft.jip11;

import com.progressoft.jip11.entity.TransactionRecord;
import com.progressoft.jip11.exceptions.DuplicateIDException;
import com.progressoft.jip11.exceptions.InvalidColumnsInFileException;
import com.progressoft.jip11.exceptions.InvalidDateException;
import com.progressoft.jip11.exceptions.InvalidPathException;
import com.progressoft.jip11.mapper.mapperimpl.JSONDefaultMapper;
import com.progressoft.jip11.reader.FileReader;
import com.progressoft.jip11.reader.readerimpl.JSONReader;
import com.progressoft.jip11.utility.DateUtility;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;

public class JSONReaderTest {
    FileReader jsonReader;

    @BeforeEach
    public void setUp() {
        jsonReader = new JSONReader(new JSONDefaultMapper());
        CurrencyStore.getCurrencyStore().addToCurrencySpecification("USD" , 2);
        CurrencyStore.getCurrencyStore().addToCurrencySpecification("JOD" , 3);
    }

    @Test
    public void canCreate() {
        new JSONReader(new JSONDefaultMapper());
    }


    @Test
    public void givenNullPath_whenRead_throwNullPointerException() {

        NullPointerException nullPointerException = Assertions.assertThrows(NullPointerException.class, () -> jsonReader.read(null));

        Assertions.assertEquals(nullPointerException.getMessage(), "Null path");
    }

    @Test
    public void givenInvalidPath_whenRead_throwInvalidPathException() {
        Path path = Path.of(".", "file" + (int) (Math.random() * 100));

        InvalidPathException invalidPathException = Assertions.assertThrows(InvalidPathException.class, () -> jsonReader.read(path));

        Assertions.assertEquals(invalidPathException.getMessage(), "File does not exist");
    }

    @Test
    public void givenDirectory_whenRead_throwInvalidPathException() throws IOException {
        Path tempDir = Files.createTempDirectory("recon-dir");

        InvalidPathException invalidPathException = Assertions.assertThrows(InvalidPathException.class, () -> jsonReader.read(tempDir));


        Assertions.assertEquals(invalidPathException.getMessage(), "Expected file found directory");
    }

    @Test
    public void givenValidEmptyFile_whenRead_throwInvalidPathException() throws IOException {
        Path tempDir = Files.createTempFile("recon-dir", ".csv");

        InvalidPathException invalidPathException = Assertions.assertThrows(InvalidPathException.class, () -> jsonReader.read(tempDir));

        Assertions.assertEquals(invalidPathException.getMessage(), "Empty file");
    }

    @Test
    public void givenValidFileWithNullRecordMapper_whenRead_throwNullPointerException() throws IOException {
        Path path = Files.createTempFile("reconciliation-online-banking-transactions-wrong-columns-length", ".json");
        copyOriginalFileToTempFile(path, "/online-banking-transactions-wrong-columns-length.json");
        jsonReader = new JSONReader(null);

        NullPointerException nullPointerException = Assertions.assertThrows(NullPointerException.class,
                () -> jsonReader.read(path));

        Assertions.assertEquals(nullPointerException.getMessage(), "Null mapper");
    }

    @Test
    public void givenValidFileWithWrongColumnsLength_whenRead_throwInvalidColumnsInFileException() throws IOException {
        Path path = Files.createTempFile("reconciliation-online-banking-transactions-wrong-columns-length", ".json");
        copyOriginalFileToTempFile(path, "/online-banking-transactions-wrong-columns-length.json");

        InvalidColumnsInFileException invalidColumnsInFileException = Assertions.assertThrows(InvalidColumnsInFileException.class,
                () -> jsonReader.read(path));

        Assertions.assertEquals(invalidColumnsInFileException.getMessage(), "Wrong columns");
    }


    @Test
    public void givenValidFileWithWrongDate_whenRead_throwInvalidDateException() throws IOException {
        Path path = Files.createTempFile("reconciliation-online-banking-transactions-with-invalid-date", ".json");
        copyOriginalFileToTempFile(path, "/online-banking-transactions-with-invalid-date.json");

        InvalidDateException invalidDateException = Assertions.assertThrows(InvalidDateException.class,
                () -> jsonReader.read(path));

        Assertions.assertEquals(invalidDateException.getMessage(), "Wrong date format");
    }

    @Test
    public void givenValidFileWithDuplicateIDS_whenRead_throwDuplicateIDException() throws IOException {
        Path path = Files.createTempFile("reconciliation-online-banking-transactions-with-duplicate-ID", ".json");
        copyOriginalFileToTempFile(path, "/online-banking-transactions-with-duplicate-ID.json");

        DuplicateIDException duplicateIDException = Assertions.assertThrows(DuplicateIDException.class, () -> jsonReader.read(path));

        Assertions.assertEquals(duplicateIDException.getMessage(), "Duplicate IDs found");
    }

    @Test
    public void givenValidFile_whenRead_thenResultIsReturned() throws IOException {
        Path path = Files.createTempFile("reconciliation-online-banking-transactions", ".json");
        copyOriginalFileToTempFile(path, "/online-banking-transactions.json");

        HashMap<String, TransactionRecord> transactionRecordHashMapExpected = new HashMap<>();
        generateTransactionRecords(transactionRecordHashMapExpected);
        HashMap<String, TransactionRecord> transactionRecordHashMapActual = jsonReader.read(path);

        Assertions.assertNotNull(transactionRecordHashMapActual);
        Assertions.assertEquals(transactionRecordHashMapExpected.size(), transactionRecordHashMapActual.size());
        Assertions.assertEquals(transactionRecordHashMapExpected, transactionRecordHashMapActual);
    }

    private void copyOriginalFileToTempFile(Path path, String originalFile) throws IOException {
        try (InputStream resourceAsStream = this.getClass().getResourceAsStream(originalFile);
             OutputStream outputStream = Files.newOutputStream(path)) {
            int read;
            while ((read = resourceAsStream.read()) != -1)
                outputStream.write(read);
        }
    }

    public static final String DATE_PATTERN = "dd/MM/yyyy";

    private void generateTransactionRecords(HashMap<String, TransactionRecord> transactionRecordHashMapExpected) {

        transactionRecordHashMapExpected.put("TR-47884222201",
                new TransactionRecord.Builder("TR-47884222201", new BigDecimal("140.00"), "USD"
                        , DateUtility.parseDate("20/01/2020", DATE_PATTERN))
                        .setPurpose("donation")
                        .build());

        transactionRecordHashMapExpected.put("TR-47884222202",
                new TransactionRecord.Builder("TR-47884222202", new BigDecimal("30.000"), "JOD"
                        , DateUtility.parseDate("22/01/2020", DATE_PATTERN))
                        .setPurpose("donation")
                        .build());

        transactionRecordHashMapExpected.put("TR-47884222203",
                new TransactionRecord.Builder("TR-47884222203", new BigDecimal("5000.000"), "JOD"
                        , DateUtility.parseDate("25/01/2020", DATE_PATTERN))
                        .setPurpose("not specified")
                        .build());
    }
}
