package com.progressoft.jip11;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.progressoft.jip11.entity.TransactionRecord;
import com.progressoft.jip11.exceptions.DuplicateIDException;
import com.progressoft.jip11.exceptions.EmptyIteratorException;
import com.progressoft.jip11.exceptions.InvalidColumnsInFileException;
import com.progressoft.jip11.exceptions.InvalidDateException;
import com.progressoft.jip11.mapper.mapperimpl.CSVDefaultMapper;
import com.progressoft.jip11.mapper.mapperimpl.JSONDefaultMapper;
import com.progressoft.jip11.mapper.RecordMapper;
import com.progressoft.jip11.utility.DateUtility;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;

public class JSONDefaultMapperTest {
    RecordMapper<JsonElement> jsonDefaultMapper;

    @BeforeEach
    public void setup() {
        jsonDefaultMapper = new JSONDefaultMapper();
        CurrencyStore.getCurrencyStore().addToCurrencySpecification("USD", 2);
        CurrencyStore.getCurrencyStore().addToCurrencySpecification("JOD", 3);
    }

    @Test
    public void canCreate() {
        new CSVDefaultMapper();
    }

    @Test
    public void givenNullTransactions_whenMap_throwNullPointerException() {
        NullPointerException nullPointerException = Assertions.assertThrows(NullPointerException.class, () -> jsonDefaultMapper.map(null));
        Assertions.assertEquals(nullPointerException.getMessage(), "Null transactions");
    }

    @Test
    public void givenEmptyTransactions_whenMap_thenThrowEmptyIteratorException() {
        ArrayList<JsonElement> arrayLists = new ArrayList<>();

        EmptyIteratorException emptyIteratorException = Assertions.assertThrows(EmptyIteratorException.class, () -> jsonDefaultMapper.map(arrayLists.iterator()));

        Assertions.assertEquals(emptyIteratorException.getMessage(), "Empty iterator");
    }

    @Test
    public void givenInvalidTransactionsColumnsLength_whenMap_thenThrowEmptyIteratorException() {
        String json = "[\n" +
                "  {\n" +
                "    \"date\": \"20/01/2020\",\n" +
                "    \"reference\": \"TR-47884222201\",\n" +
                "    \"amount\": \"140.00\",\n" +
                "    \"currencyCode\": \"USD\"\n" +
                "  },\n" +
                "  {\n" +
                "    \"date\": \"22/01/2020\",\n" +
                "    \"reference\": \"TR-47884222202\",\n" +
                "    \"amount\": \"30.000\",\n" +
                "    \"currencyCode\": \"JOD\",\n" +
                "    \"purpose\": \"donation\"\n" +
                "  },\n" +
                "  {\n" +
                "    \"date\": \"25/01/2020\",\n" +
                "    \"reference\": \"TR-47884222203\",\n" +
                "    \"amount\": \"5000.000\",\n" +
                "    \"currencyCode\": \"JOD\",\n" +
                "    \"purpose\": \"not specified\"\n" +
                "  }\n" +
                "]";

        Object obj = JsonParser.parseString(json);
        JsonArray transactionList = (JsonArray) obj;

        InvalidColumnsInFileException invalidColumnsInFileException = Assertions.assertThrows(InvalidColumnsInFileException.class, () -> jsonDefaultMapper.map(transactionList.iterator()));

        Assertions.assertEquals(invalidColumnsInFileException.getMessage(), "Wrong columns");
    }

    @Test
    public void givenDuplicateIDS_whenMap_thenThrowDuplicateIDException() {
        String json = "[\n" +
                "  {\n" +
                "    \"date\": \"20/01/2020\",\n" +
                "    \"reference\": \"TR-47884222201\",\n" +
                "    \"amount\": \"140.00\",\n" +
                "    \"currencyCode\": \"USD\",\n" +
                "    \"purpose\": \"donation\"\n" +
                "  },\n" +
                "  {\n" +
                "    \"date\": \"03/02/2020\",\n" +
                "    \"reference\": \"TR-47884222201\",\n" +
                "    \"amount\": \"60.000\",\n" +
                "    \"currencyCode\": \"JOD\",\n" +
                "    \"purpose\": \"\"\n" +
                "  },\n" +
                "  {\n" +
                "    \"date\": \"10/02/2020\",\n" +
                "    \"reference\": \"TR-47884222206\",\n" +
                "    \"amount\": \"500.00\",\n" +
                "    \"currencyCode\": \"USD\",\n" +
                "    \"purpose\": \"general\"\n" +
                "  },\n" +
                "  {\n" +
                "    \"date\": \"22/01/2020\",\n" +
                "    \"reference\": \"TR-47884222202\",\n" +
                "    \"amount\": \"30.000\",\n" +
                "    \"currencyCode\": \"JOD\",\n" +
                "    \"purpose\": \"donation\"\n" +
                "  },\n" +
                "  {\n" +
                "    \"date\": \"14/02/2020\",\n" +
                "    \"reference\": \"TR-47884222217\",\n" +
                "    \"amount\": \"12000.000\",\n" +
                "    \"currencyCode\": \"JOD\",\n" +
                "    \"purpose\": \"salary\"\n" +
                "  },\n" +
                "  {\n" +
                "    \"date\": \"25/01/2020\",\n" +
                "    \"reference\": \"TR-47884222203\",\n" +
                "    \"amount\": \"5000.000\",\n" +
                "    \"currencyCode\": \"JOD\",\n" +
                "    \"purpose\": \"not specified\"\n" +
                "  },\n" +
                "  {\n" +
                "    \"date\": \"12/01/2020\",\n" +
                "    \"reference\": \"TR-47884222245\",\n" +
                "    \"amount\": \"420.00\",\n" +
                "    \"currencyCode\": \"USD\",\n" +
                "    \"purpose\": \"loan\"\n" +
                "  }\n" +
                "]";

        Object obj = JsonParser.parseString(json);
        JsonArray transactionList = (JsonArray) obj;

        DuplicateIDException duplicateIDException = Assertions.assertThrows(DuplicateIDException.class, () -> jsonDefaultMapper.map(transactionList.iterator()));

        Assertions.assertEquals(duplicateIDException.getMessage(), "Duplicate IDs found");
    }

    @Test
    public void givenInvalidDate_whenMap_thenThrowInvalidDateException() {
        String json = "[\n" +
                "  {\n" +
                "    \"date\": \"20/01/2020\",\n" +
                "    \"reference\": \"TR-47884222201\",\n" +
                "    \"amount\": \"140.00\",\n" +
                "    \"currencyCode\": \"USD\",\n" +
                "    \"purpose\": \"donation\"\n" +
                "  },\n" +
                "  {\n" +
                "    \"date\": \"2020/02/06\",\n" +
                "    \"reference\": \"TR-47884222205\",\n" +
                "    \"amount\": \"60.000\",\n" +
                "    \"currencyCode\": \"JOD\",\n" +
                "    \"purpose\": \"\"\n" +
                "  },\n" +
                "  {\n" +
                "    \"date\": \"10/02/2020\",\n" +
                "    \"reference\": \"TR-47884222206\",\n" +
                "    \"amount\": \"500.00\",\n" +
                "    \"currencyCode\": \"USD\",\n" +
                "    \"purpose\": \"general\"\n" +
                "  },\n" +
                "  {\n" +
                "    \"date\": \"22/01/2020\",\n" +
                "    \"reference\": \"TR-47884222202\",\n" +
                "    \"amount\": \"30.000\",\n" +
                "    \"currencyCode\": \"JOD\",\n" +
                "    \"purpose\": \"donation\"\n" +
                "  },\n" +
                "  {\n" +
                "    \"date\": \"14/02/2020\",\n" +
                "    \"reference\": \"TR-47884222217\",\n" +
                "    \"amount\": \"12000.000\",\n" +
                "    \"currencyCode\": \"JOD\",\n" +
                "    \"purpose\": \"salary\"\n" +
                "  },\n" +
                "  {\n" +
                "    \"date\": \"25/01/2020\",\n" +
                "    \"reference\": \"TR-47884222203\",\n" +
                "    \"amount\": \"5000.000\",\n" +
                "    \"currencyCode\": \"JOD\",\n" +
                "    \"purpose\": \"not specified\"\n" +
                "  },\n" +
                "  {\n" +
                "    \"date\": \"12/01/2020\",\n" +
                "    \"reference\": \"TR-47884222245\",\n" +
                "    \"amount\": \"420.00\",\n" +
                "    \"currencyCode\": \"USD\",\n" +
                "    \"purpose\": \"loan\"\n" +
                "  }\n" +
                "]";
        Object obj = JsonParser.parseString(json);
        JsonArray transactionList = (JsonArray) obj;


        InvalidDateException invalidDateException = Assertions.assertThrows(InvalidDateException.class, () -> jsonDefaultMapper.map(transactionList.iterator()));

        Assertions.assertEquals(invalidDateException.getMessage(), "Wrong date format");
    }

    @Test
    public void givenValidTransactions_whenMap_thenResultIsReturned() {
        String json = "[\n" +
                "  {\n" +
                "    \"date\": \"20/01/2020\",\n" +
                "    \"reference\": \"1111\",\n" +
                "    \"amount\": \"140.00\",\n" +
                "    \"currencyCode\": \"USD\",\n" +
                "    \"purpose\": \"donation\"\n" +
                "  },\n" +
                "  {\n" +
                "    \"date\": \"22/01/2020\",\n" +
                "    \"reference\": \"2222\",\n" +
                "    \"amount\": \"30.000\",\n" +
                "    \"currencyCode\": \"JOD\",\n" +
                "    \"purpose\": \"donation\"\n" +
                "  },\n" +
                "  {\n" +
                "    \"date\": \"25/01/2020\",\n" +
                "    \"reference\": \"3333\",\n" +
                "    \"amount\": \"5000.000\",\n" +
                "    \"currencyCode\": \"JOD\",\n" +
                "    \"purpose\": \"not specified\"\n" +
                "  }\n" +
                "]";
        Object obj = JsonParser.parseString(json);
        JsonArray transactionList = (JsonArray) obj;

        HashMap<String, TransactionRecord> transactionRecordHashMap = jsonDefaultMapper.map(transactionList.iterator());

        Assertions.assertNotNull(transactionRecordHashMap);
        Assertions.assertEquals(transactionRecordHashMap.size(), 3);

        String dateFormat = "dd/MM/yyyy";

        Assertions.assertNotNull(transactionRecordHashMap.get("1111"));
        Assertions.assertEquals(transactionRecordHashMap.get("1111").getId(), "1111");
        Assertions.assertEquals(transactionRecordHashMap.get("1111").getPurpose(), "donation");
        Assertions.assertEquals(transactionRecordHashMap.get("1111").getAmount(), new BigDecimal("140.00"));
        Assertions.assertEquals(transactionRecordHashMap.get("1111").getDate(), DateUtility.parseDate("20/01/2020", dateFormat));
        Assertions.assertEquals(transactionRecordHashMap.get("1111").getCurrency(), "USD");


        Assertions.assertNotNull(transactionRecordHashMap.get("2222"));
        Assertions.assertEquals(transactionRecordHashMap.get("2222").getId(), "2222");
        Assertions.assertEquals(transactionRecordHashMap.get("2222").getPurpose(), "donation");
        Assertions.assertEquals(transactionRecordHashMap.get("2222").getAmount(), new BigDecimal("30.000"));
        Assertions.assertEquals(transactionRecordHashMap.get("2222").getDate(), DateUtility.parseDate("22/01/2020", dateFormat));
        Assertions.assertEquals(transactionRecordHashMap.get("2222").getCurrency(), "JOD");


        Assertions.assertNotNull(transactionRecordHashMap.get("3333"));
        Assertions.assertEquals(transactionRecordHashMap.get("3333").getId(), "3333");
        Assertions.assertEquals(transactionRecordHashMap.get("3333").getPurpose(), "not specified");
        Assertions.assertEquals(transactionRecordHashMap.get("3333").getAmount(), new BigDecimal("5000.000"));
        Assertions.assertEquals(transactionRecordHashMap.get("3333").getDate(), DateUtility.parseDate("25/01/2020", dateFormat));
        Assertions.assertEquals(transactionRecordHashMap.get("3333").getCurrency(), "JOD");
    }
}
