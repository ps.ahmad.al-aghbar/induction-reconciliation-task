package com.progressoft.jip11.mapper.mapperimpl;

import com.progressoft.jip11.entity.TransactionRecord;
import com.progressoft.jip11.exceptions.EmptyIteratorException;
import com.progressoft.jip11.exceptions.InvalidColumnsInFileException;
import com.progressoft.jip11.exceptions.InvalidDateException;
import com.progressoft.jip11.mapper.RecordMapper;
import com.progressoft.jip11.utility.DateUtility;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;

public class CSVDefaultMapper implements RecordMapper<String[]> {
    public static final int COLUMNS_LENGTH = 7;
    public static final String DATE_PATTERN = "yyyy-MM-dd";

    @Override
    public HashMap<String, TransactionRecord> map(Iterator<String[]> transactions) {
        if (transactions == null)
            throw new NullPointerException("Null transactions");
        if (!transactions.hasNext())
            throw new EmptyIteratorException("Empty iterator");
        HashMap<String, TransactionRecord> transactionRecordsHashMap = new LinkedHashMap<>();
        String[] transaction;
        while (transactions.hasNext()) {
            transaction = transactions.next();
            if (transaction.length != COLUMNS_LENGTH)
                throw new InvalidColumnsInFileException("Wrong columns");
            RecordMapper.checkDuplicatesID(transactionRecordsHashMap, transaction[0]);
            transactionRecordsHashMap.put(transaction[0], getTransactionRecord(transaction));
        }
        return transactionRecordsHashMap;
    }


    @Override
    public TransactionRecord getTransactionRecord(String[] transaction) {
        String id = transaction[0];
        String description = transaction[1];
        BigDecimal amount = new BigDecimal(transaction[2]);
        String currency = transaction[3];
        String purpose = transaction[4];
        LocalDate date = getDate(transaction[5]);
        String type = transaction[6];

        return new TransactionRecord.Builder(id, amount, currency, date)
                .setPurpose(purpose)
                .setDescription(description)
                .setType(type)
                .build();
    }

    public LocalDate getDate(String date) {
        String[] dateSplit = date.split("-");
        if (dateSplit.length != 3)
            throw new InvalidDateException("Wrong date format");
        if (!(dateSplit[0].length() == 4 && dateSplit[1].length() == 2 && dateSplit[2].length() == 2))
            throw new InvalidDateException("Wrong date format");
        return new DateUtility().parseDate(date, DATE_PATTERN);
    }
}
