package com.progressoft.jip11.reader;

import com.progressoft.jip11.entity.TransactionRecord;
import com.progressoft.jip11.mapper.RecordMapper;

import java.nio.file.Path;
import java.util.HashMap;

public interface FileReader{
    HashMap<String, TransactionRecord> read(Path path);
}
