package com.progressoft.jip11.exceptions;

public class DuplicateIDException extends RuntimeException{

    public DuplicateIDException(String message) {
        super(message);
    }

    public DuplicateIDException(String message, Exception e) {
        super(message, e);
    }
}
