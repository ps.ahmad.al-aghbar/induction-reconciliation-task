package com.progressoft.jip11.writer;

import com.progressoft.jip11.writer.writeimpl.WriteRequest;

public interface FileWriter {
    void write(WriteRequest writeRequest);
}
