package com.progressoft.jip11.exceptions;

public class InvalidColumnsInFileException extends RuntimeException {

    public InvalidColumnsInFileException(String message) {
        super(message);
    }

    public InvalidColumnsInFileException(String message, Exception e) {
        super(message, e);
    }
}
