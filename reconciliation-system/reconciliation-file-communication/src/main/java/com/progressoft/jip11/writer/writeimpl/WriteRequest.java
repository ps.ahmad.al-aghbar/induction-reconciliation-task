package com.progressoft.jip11.writer.writeimpl;

import com.progressoft.jip11.entity.FileRecord;
import com.progressoft.jip11.fileheader.FileHeader;

import java.nio.file.Path;
import java.util.ArrayList;

public class WriteRequest {
    private final Path path;
    private final ArrayList<FileRecord> fileRecords;
    private final FileHeader fileHeader;

    public WriteRequest(Path path, ArrayList<FileRecord> fileRecords, FileHeader fileHeader) {
        this.path = path;
        this.fileRecords = fileRecords;
        this.fileHeader = fileHeader;
    }

    public Path getPath() {
        return path;
    }

    public ArrayList<FileRecord> getFileRecords() {
        return fileRecords;
    }

    public FileHeader getFileHeader() {
        return fileHeader;
    }
}
