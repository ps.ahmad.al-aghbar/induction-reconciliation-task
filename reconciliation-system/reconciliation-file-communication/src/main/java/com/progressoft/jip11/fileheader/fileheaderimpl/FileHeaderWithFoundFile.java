package com.progressoft.jip11.fileheader.fileheaderimpl;

import com.progressoft.jip11.fileheader.FileHeader;

public class FileHeaderWithFoundFile implements FileHeader {
    @Override
    public String[] getHeader() {
        return new String[]{"found in file", "transaction id", "amount", "currency code", "value date"};
    }

    @Override
    public String[] getMapper() {
        return new String[]{"fileTag", "id", "amount", "currency", "date"};
    }
}
