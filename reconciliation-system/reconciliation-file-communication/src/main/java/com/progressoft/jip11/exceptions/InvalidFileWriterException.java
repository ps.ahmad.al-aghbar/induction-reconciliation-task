package com.progressoft.jip11.exceptions;

public class InvalidFileWriterException extends RuntimeException {
    public InvalidFileWriterException(String message, Exception e) {
        super(message, e);
    }
}
