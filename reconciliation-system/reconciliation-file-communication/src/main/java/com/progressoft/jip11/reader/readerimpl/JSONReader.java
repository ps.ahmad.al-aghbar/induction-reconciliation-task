package com.progressoft.jip11.reader.readerimpl;

import com.google.gson.*;
import com.progressoft.jip11.entity.TransactionRecord;
import com.progressoft.jip11.exceptions.InvalidColumnsInFileException;
import com.progressoft.jip11.exceptions.InvalidFileReaderException;
import com.progressoft.jip11.mapper.RecordMapper;
import com.progressoft.jip11.reader.FileReader;
import com.progressoft.jip11.validator.FileValidator;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;

public class JSONReader implements FileReader {
    RecordMapper<JsonElement> recordMapper;

    public JSONReader(RecordMapper<JsonElement> recordMapper) {
        this.recordMapper = recordMapper;
    }

    @Override
    public HashMap<String, TransactionRecord> read(Path path) {
        FileValidator.validateReadPath(path);
        if (recordMapper == null)
            throw new NullPointerException("Null mapper");

        try (BufferedReader bufferedReader = Files.newBufferedReader(path)) {
            Object obj = JsonParser.parseReader(bufferedReader);
            JsonArray transactionList = (JsonArray) obj;
            return recordMapper.map(transactionList.iterator());

        } catch (IOException exception) {
            throw new InvalidFileReaderException(exception.getMessage(), exception);
        }
    }
}
