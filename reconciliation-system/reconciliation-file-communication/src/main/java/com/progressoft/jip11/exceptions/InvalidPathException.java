package com.progressoft.jip11.exceptions;

import java.io.IOException;

public class InvalidPathException extends RuntimeException {
    public InvalidPathException(String message) {
        super(message);
    }

    public InvalidPathException(IOException exception) {
        super(exception);
    }
}
