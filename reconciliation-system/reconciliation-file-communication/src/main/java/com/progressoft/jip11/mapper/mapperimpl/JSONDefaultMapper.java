package com.progressoft.jip11.mapper.mapperimpl;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.progressoft.jip11.CurrencyStore;
import com.progressoft.jip11.entity.TransactionRecord;
import com.progressoft.jip11.exceptions.EmptyIteratorException;
import com.progressoft.jip11.exceptions.InvalidColumnsInFileException;
import com.progressoft.jip11.exceptions.InvalidDateException;
import com.progressoft.jip11.mapper.RecordMapper;
import com.progressoft.jip11.utility.DateUtility;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;

public class JSONDefaultMapper implements RecordMapper<JsonElement> {
    public static final int COLUMNS_LENGTH = 5;
    public static final String DATE_PATTERN = "dd/MM/yyyy";


    @Override
    public HashMap<String, TransactionRecord> map(Iterator<JsonElement> transactions) {
        if (transactions == null)
            throw new NullPointerException("Null transactions");
        if (!transactions.hasNext()) {
            throw new EmptyIteratorException("Empty iterator");
        }
        HashMap<String, TransactionRecord> transactionRecordHashMap = new LinkedHashMap<>();
        TransactionRecord transactionRecord;
        while (transactions.hasNext()) {
            transactionRecord = getTransactionRecord(transactions.next());
            RecordMapper.checkDuplicatesID(transactionRecordHashMap, transactionRecord.getId());
            transactionRecordHashMap.put(transactionRecord.getId(), transactionRecord);
        }

        return transactionRecordHashMap;
    }

    @Override
    public TransactionRecord getTransactionRecord(JsonElement transaction) {
        JsonObject transactionRecord = (JsonObject) transaction;

        if (transactionRecord.size() != COLUMNS_LENGTH)
            throw new InvalidColumnsInFileException("Wrong columns");


        BigDecimal amount = transactionRecord.get("amount").getAsBigDecimal();
        String currency = transactionRecord.get("currencyCode").getAsString();
        LocalDate date = getDate(transactionRecord.get("date").getAsString());
        String id = transactionRecord.get("reference").getAsString();
        String purpose = transactionRecord.get("purpose").getAsString();


        return new TransactionRecord.Builder(id, amount, currency, date)
                .setPurpose(purpose)
                .build();
    }

    private LocalDate getDate(String date) {
        String[] dateSplit = date.split("/");
        if (dateSplit.length != 3)
            throw new InvalidDateException("Wrong date format");
        if (!(dateSplit[0].length() == 2 && dateSplit[1].length() == 2 && dateSplit[2].length() == 4))
            throw new InvalidDateException("Wrong date format");
        return new DateUtility().parseDate(date, DATE_PATTERN);
    }
}
