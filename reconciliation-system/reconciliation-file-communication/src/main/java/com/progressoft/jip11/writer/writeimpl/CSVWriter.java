package com.progressoft.jip11.writer.writeimpl;

import com.opencsv.bean.ColumnPositionMappingStrategy;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;
import com.progressoft.jip11.entity.FileRecord;
import com.progressoft.jip11.exceptions.InvalidFileWriterException;
import com.progressoft.jip11.writer.FileWriter;

import java.io.IOException;
import java.nio.file.Files;
import java.util.Collections;

public class CSVWriter implements FileWriter {

    @Override
    public void write(WriteRequest writeRequest) {
        if (writeRequest.getPath() == null)
            throw new NullPointerException("Null path");
        if (writeRequest.getFileRecords() == null)
            throw new NullPointerException("Null file records");


        try (com.opencsv.CSVWriter writer = new com.opencsv.CSVWriter(Files.newBufferedWriter(writeRequest.getPath()))) {
            ColumnPositionMappingStrategy<FileRecord> mappingStrategy = new ColumnPositionMappingStrategy<FileRecord>();

            mappingStrategy.setType(FileRecord.class);
            mappingStrategy.setColumnMapping(writeRequest.getFileHeader().getMapper());

            StatefulBeanToCsv<FileRecord> csvWriter = new StatefulBeanToCsvBuilder<FileRecord>(writer)
                    .withMappingStrategy(mappingStrategy)
                    .withSeparator(com.opencsv.CSVWriter.DEFAULT_SEPARATOR)
                    .withQuotechar(com.opencsv.CSVWriter.NO_QUOTE_CHARACTER)
                    .withEscapechar(com.opencsv.CSVWriter.DEFAULT_ESCAPE_CHARACTER)
                    .withLineEnd(com.opencsv.CSVWriter.DEFAULT_LINE_END)
                    .withOrderedResults(true)
                    .build();

            writer.writeAll(Collections.singleton(writeRequest.getFileHeader().getHeader()));

            csvWriter.write(writeRequest.getFileRecords());
        } catch (IOException | CsvRequiredFieldEmptyException | CsvDataTypeMismatchException e) {
            throw new InvalidFileWriterException(e.getMessage(), e);
        }

    }
}
