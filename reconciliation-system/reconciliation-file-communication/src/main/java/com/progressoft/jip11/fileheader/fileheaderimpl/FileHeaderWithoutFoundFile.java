package com.progressoft.jip11.fileheader.fileheaderimpl;

import com.progressoft.jip11.fileheader.FileHeader;

public class FileHeaderWithoutFoundFile implements FileHeader {
    @Override
    public String[] getHeader() {
        return new String[]{"transaction id", "amount", "currency code", "value date"};
    }

    @Override
    public String[] getMapper() {
        return new String[]{"id", "amount", "currency", "date"};
    }
}
