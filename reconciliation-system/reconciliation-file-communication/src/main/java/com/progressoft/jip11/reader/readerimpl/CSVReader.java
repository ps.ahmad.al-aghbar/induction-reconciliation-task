package com.progressoft.jip11.reader.readerimpl;

import com.opencsv.exceptions.CsvValidationException;
import com.progressoft.jip11.entity.TransactionRecord;
import com.progressoft.jip11.exceptions.DuplicateIDException;
import com.progressoft.jip11.exceptions.InvalidColumnsInFileException;
import com.progressoft.jip11.exceptions.InvalidFileReaderException;
import com.progressoft.jip11.mapper.RecordMapper;
import com.progressoft.jip11.reader.FileReader;
import com.progressoft.jip11.validator.FileValidator;
import com.progressoft.jip11.utility.DateUtility;

import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;

public class CSVReader implements FileReader {
    RecordMapper<String[]> recordMapper;

    public CSVReader(RecordMapper<String[]> recordMapper) {
        this.recordMapper = recordMapper;
    }

    @Override
    public HashMap<String, TransactionRecord> read(Path path) {
        FileValidator.validateReadPath(path);
        if (recordMapper == null)
            throw new NullPointerException("Null mapper");

        try (com.opencsv.CSVReader reader = new com.opencsv.CSVReader(Files.newBufferedReader(path))) {
            skipHeader(reader);
            return recordMapper.map(reader.iterator());
        } catch (IOException | CsvValidationException exception) {
            throw new InvalidFileReaderException(exception.getMessage(), exception);
        }
    }

    private void skipHeader(com.opencsv.CSVReader reader) throws IOException, CsvValidationException {
        reader.readNext();
    }

}
