package com.progressoft.jip11.exceptions;

public class InvalidFileReaderException extends RuntimeException {
    public InvalidFileReaderException(String message, Exception exception) {
        super(message, exception);
    }
}
