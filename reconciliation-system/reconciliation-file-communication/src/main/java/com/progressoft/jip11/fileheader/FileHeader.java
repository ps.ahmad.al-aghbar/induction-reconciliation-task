package com.progressoft.jip11.fileheader;

public interface FileHeader {
    String[] getHeader();
    String[] getMapper();
}
