package com.progressoft.jip11.exceptions;

public class EmptyIteratorException extends RuntimeException {

    public EmptyIteratorException(String message) {
        super(message);
    }
}
