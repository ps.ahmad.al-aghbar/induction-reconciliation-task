package com.progressoft.jip11.mapper;

import com.progressoft.jip11.entity.TransactionRecord;
import com.progressoft.jip11.exceptions.DuplicateIDException;

import java.util.HashMap;
import java.util.Iterator;

public interface RecordMapper<RECORD> {

    HashMap<String, TransactionRecord> map(Iterator<RECORD> transactions);

    TransactionRecord getTransactionRecord(RECORD transaction);

    static void checkDuplicatesID(HashMap<String, TransactionRecord> transactionRecordsHashMap, String id) {
        if (transactionRecordsHashMap.containsKey(id))
            throw new DuplicateIDException("Duplicate IDs found");
    }
}
