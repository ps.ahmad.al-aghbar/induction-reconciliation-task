package com.progressoft.jip11.validator;

import com.progressoft.jip11.exceptions.InvalidPathException;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class FileValidator {
    public static void validateReadPath(Path path) {
        if (path == null)
            throw new NullPointerException("Null path");
        if (!Files.exists(path))
            throw new InvalidPathException("File does not exist");
        if (Files.isDirectory(path))
            throw new InvalidPathException("Expected file found directory");
        try {
            if (Files.size(path) == 0)
                throw new InvalidPathException("Empty file");
        } catch (IOException exception) {
            throw new InvalidPathException(exception);
        }
    }
}
